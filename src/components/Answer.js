import '../styles/Answer.css'

import { useState } from 'react'

function Answer() {
    const title = "Bravo, vous avez déverouillé l'accès au secret ultime:"
    const content = ["Nous savons que vous ne portez pas ces âges dans votre coeur", "Mais nous voulions partager avec vous notre bonheur", "Aujourd'hui pas plus gros qu'un abricot", "En Mars, il pèsera quelques kilos!"]

    return (
        <div className=''>
            <h1 className=''>{title}</h1>
            {content.map((line) => (
                <p>{line}</p>

            ))}

        </div>
    )
}

export default Answer