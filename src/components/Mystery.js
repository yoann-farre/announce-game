import '../styles/Mystery.css'

import { useState } from 'react'
import { useHistory } from 'react-router-dom';

function Mystery() {

    const history = useHistory();
    const [inputValue, setInputValue] = useState("")

    const title = "Enigme"
    const content = ["Je n'ai jamais été et il en restera ainsi", "Insaisissable, toutes et tous je fuis", "Et pourtant j'entretiens la foi", "De toutes et tous qui espèrent en moi"]
    const answer = "demain"

    const handledValues = new Map([
        ["dieu", "Je devrais accepter cette réponse mais ce n'est pas celle attendue"],
        ["avenir", "Vous vous rapprochez"],
        ["futur", "Un peu moins loin"]
    ])
    

    function checkValue() {
        const transformedValue = inputValue.toLowerCase()
        if (transformedValue === answer) {
            setInputValue("")
            history.push("/petit-abricot");
            return
        } else {
            const value = handledValues.get(transformedValue)
            if (value !== undefined){
                setInputValue("")
                alert(value)
                return 
            }
        }

        alert("Mauvaise réponse. Soyez persévérant!")
        setInputValue("")
    }

    return (
        <div className=''>
            <h1 className=''>{title}</h1>
            {content.map((line) => (
                <p>{line}</p>

            ))}
            <p style={{ fontWeight: "bold" }}>Qui suis je?</p>

            <textarea
                value={inputValue}
                onChange={(e) => setInputValue(e.target.value)}
            />
            <button onClick={checkValue}>Valider ma réponse</button>
        </div>
    )
}

export default Mystery