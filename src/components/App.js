import logo from './../logo-final.svg';
import './../styles/App.css';
import Mystery from "./Mystery"
import Answer from "./Answer"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Router>
          <div>
            <hr />

            <Switch>
              <Route exact path="/">
                <Mystery />
              </Route>
              <Route path="/petit-abricot">
                <Answer />
              </Route>
            </Switch>
          </div>
        </Router>

      </header>
    </div>
  );
}

export default App;
